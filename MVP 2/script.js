urlmessages = "https://treinamentoajax.herokuapp.com/messages"
url = "https://treinamentoajax.herokuapp.com/"

//Generic
idlist = []

//POST
var nome = document.querySelector("#nome")
var mensagem = document.querySelector("#mensagem")
var enviar = document.querySelector("#enviar")
var lista = document.querySelector("#lista")

//GET
var obter = document.querySelector("#obter")
//GET específico
var obterid = document.querySelector("#obterid")
var id = document.querySelector("#id")


//POST - Enviar
enviar.addEventListener("click", e =>{
    if(nome.value == "" || mensagem.value == ""){
        console.log("Campos não podem estar vazio")
    }else{
        var fetchBody = {
            "message":{
                "name": nome.value,
                "message": mensagem.value
            }
        }
        var fetchConfig = {
            method: "POST",
            headers: {"Content-Type":"application/JSON"},
            body: JSON.stringify(fetchBody)
        }      
        fetch(urlmessages, fetchConfig)
        nome.value = ""
        mensagem.value = ""
    }
    
})

//GET
obter.addEventListener("click", e =>{
    clear()
    fetch(urlmessages).then(response => response.json())
    .then(lista => {
        for(x of lista){
            createDiv(x)
            idlist.push(x.id)
        }
    })
})
//Cria uma div com as infos
function createDiv(m){
    var divblock = document.createElement("div")
    var genericdiv = document.createElement("div")
    var del = document.createElement("button")
    var id = document.createElement("span")

    id.innerHTML = m.id
    id.style.display = "none"

    divblock.classList.add("block")

    del.classList.add("deletar")
    del.innerHTML = "Deletar"
    del.style.height = "21px"

    var nome = document.createElement("h2")
    nome.innerHTML = m.name

    var mensagem = document.createElement("p")
    mensagem.innerHTML = m.message

    genericdiv.appendChild(nome)
    genericdiv.appendChild(del)
    divblock.appendChild(genericdiv)
    divblock.appendChild(mensagem)
    del.appendChild(id)
    lista.appendChild(divblock)
}
//clear na lista de mensagens
function clear(){
    for(x of document.querySelectorAll(".block")){
        x.remove()
    }
    idlist = []
}

//GET específico
obterid.addEventListener("click", e => {
    var idnum = id.value
    if(idnum == ""){
        console.log("Não pode estar vazio")
    }else{
        idurl = urlmessages + "/" + idnum
        clear()
        idlist.push(idnum)

        fetch(idurl).then(response => response.json())
        .then(m => createDiv(m))
        .catch(e => console.log("Id inexistente"))//Não existe, substituir por .invalid
    }
})

//DELETE
lista.addEventListener("click", e =>{
    if(e.target.classList[0] == "deletar"){
        e.path[2].classList.add("target")
        for(x in lista.children){
            if(lista.children[x].classList.contains("target") == true){
                y = idlist[x]
                fetch(urlmessages + "/" + y,{method:"DELETE"})
                lista.children[x].remove()
            }
        }  
    }
})

var a = {
    id: 2,
    name: "Rafael Carrilho",
    message: "Eu vou ser o hokage",
    created_at: "2020-07-19T14:41:40.852Z",
    updated_at: "2020-07-19T14:41:40.852Z"}



