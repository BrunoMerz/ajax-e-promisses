urlmessages = "https://treinamentoajax.herokuapp.com/messages"
url = "https://treinamentoajax.herokuapp.com/"

//Generic
idlist = []

//POST
var nome = document.querySelector("#nome")
var mensagem = document.querySelector("#mensagem")
var enviar = document.querySelector("#enviar")
var lista = document.querySelector("#lista")

//GET
var obter = document.querySelector("#obter")
//GET específico
var obterid = document.querySelector("#obterid")
var id = document.querySelector("#id")


//POST - Enviar
enviar.addEventListener("click", e =>{
    if(nome.value == "" || mensagem.value == ""){
        console.log("Campos não podem estar vazio")
    }else{
        enviar.style.cursor = "wait"
        document.body.style.cursor = "progress"
        var fetchBody = {
            "message":{
                "name": nome.value,
                "message": mensagem.value
            }
        }
        var fetchConfig = {
            method: "POST",
            headers: {"Content-Type":"application/JSON"},
            body: JSON.stringify(fetchBody)
        }      
        fetch(urlmessages, fetchConfig).then(e => {
            enviar.style.cursor = "unset"
            document.body.style.cursor = "unset"
        })
        nome.value = ""
        mensagem.value = ""
    }
    
})

//GET
obter.addEventListener("click", e =>{
    clear()
    document.body.style.cursor = "progress"
    obter.style.cursor = "wait"
    fetch(urlmessages).then(response => response.json())
    .then(lista => {
        for(x of lista){
            createDiv(x)
            idlist.push(x.id)
        }
        document.body.style.cursor = "unset"
        obter.style.cursor = "unset"
    })
})
//Cria uma div com as infos
function createDiv(m){
    var divblock = document.createElement("div")
    var genericdiv = document.createElement("div")
    var del = document.createElement("button")
    var id = document.createElement("span")
    var edit = document.createElement("button")
    var buttondiv = document.createElement("div")

    id.innerHTML = m.id
    id.style.display = "none"

    divblock.classList.add("block")

    del.classList.add("deletar")
    del.innerHTML = '<img src="img/trash.png">'

    edit.classList.add("editar")
    edit.innerHTML = '<img src="img/edit.png">'

    var nome = document.createElement("h2")
    nome.innerHTML = m.name

    var mensagem = document.createElement("p")
    mensagem.innerHTML = m.message

    genericdiv.appendChild(nome)
    buttondiv.appendChild(del)
    buttondiv.appendChild(edit)
    genericdiv.appendChild(buttondiv)
    divblock.appendChild(genericdiv)
    divblock.appendChild(mensagem)
    del.appendChild(id)
    lista.appendChild(divblock)
}
//clear na lista de mensagens
function clear(){
    for(x of document.querySelectorAll(".block")){
        x.remove()
    }
    idlist = []
}

//GET específico
obterid.addEventListener("click", e => {
    var idnum = id.value
    if(idnum == ""){
        console.log("Não pode estar vazio")
    }else{
        idurl = urlmessages + "/" + idnum
        clear()
        idlist.push(idnum)

        fetch(idurl).then(response => response.json())
        .then(m => createDiv(m))
        .catch(e => console.log("Id inexistente"))//Não existe, substituir por .invalid
    }
})

//DELETE
lista.addEventListener("click", e =>{
    if(e.target.parentElement.classList[0] == "deletar"){
        document.body.style.cursor = "progress"
        e.path[4].classList.add("target")
        for(x in lista.children){
            if(lista.children[x].classList.contains("target") == true){
                y = idlist[x]
                fetch(urlmessages + "/" + y,{method:"DELETE"}).then(document.body.style.cursor = "unset").then(lista.children[x].remove())
            }
        }  
    }
})

//EDIT
lista.addEventListener("click", e =>{
    if(e.target.parentElement.classList[0] == "editar"){
        if(e.path[4].children[2] != undefined){
            e.path[4].children[2].remove()
        }
        createEdit(e.path[4])
        }
})
//COMFIRM EDIT
lista.addEventListener("click", e =>{
    if(e.target.classList[0] == "salvar"){
        e.path[3].classList.add("salvar")
        for(x in lista.children){
            if(lista.children[x].classList.contains("salvar") == true){
                if(e.path[2].children[0].children[1].value != "" && e.path[2].children[1].children[1].value != ""){
                    y = idlist[x]

                    var fetchBody = {
                        "message":{
                            "name": e.path[2].children[0].children[1].value,
                            "message": e.path[2].children[1].children[1].value
                        }
                    }
                    console.log(fetchBody)
                    var fetchConfig={
                        method:"PUT",
                        headers:{"Content-Type": "application/JSON"},
                        body: JSON.stringify(fetchBody)
                    }
                    
                    fetch(urlmessages + "/" + y,fetchConfig)
                    e.path[2].remove()


                }else{
                    console.log("Não pode estar vazio")
                }
            }
        }  
    }
})
//CANCEL EDIT
lista.addEventListener("click", e =>{
    if(e.target.classList[0] == "cancelar"){
        e.path[2].remove()
    }
})

//Creat layout for editing messages
function createEdit(div){
    var editDiv = document.createElement("div")
    editDiv.classList.add("editardiv")

    //--------
    var div1 = document.createElement("div")
    var nomep = document.createElement("p")
    nomep.innerHTML = "Nome:"

    var nomebox = document.createElement("input")
    nomebox.value = div.children[0].children[0].innerHTML
    nomebox.classList.add("nomeedit")

    div1.appendChild(nomep)  //
    div1.appendChild(nomebox)//
    //---------------------------
    var div2 = document.createElement("div")
    var messagep = document.createElement("p")
    messagep.innerHTML = "Mensagem:"
    var messagebox = document.createElement("textarea")
    messagebox.value = div.children[1].innerHTML
    messagebox.classList.add("messageedit")


    div2.appendChild(messagep)  //
    div2.appendChild(messagebox)//
    //----------------------------
    var div3 = document.createElement("div")

    //---------------------------
    var edit = document.createElement("button")
    edit.innerHTML = "Salvar"
    edit.classList.add("salvar")

    var cancel = document.createElement("button")
    cancel.innerHTML = "Cancelar"
    cancel.classList.add("cancelar")
    
    div3.appendChild(edit)      //
    div3.appendChild(cancel)    //


    //--------------------------
    editDiv.appendChild(div1)
    editDiv.appendChild(div2)
    editDiv.appendChild(div3)
    div.appendChild(editDiv)
}

var a = {
    id: 2,
    name: "Rafael Carrilho",
    message: "Eu vou ser o hokage",
    created_at: "2020-07-19T14:41:40.852Z",
    updated_at: "2020-07-19T14:41:40.852Z"}

