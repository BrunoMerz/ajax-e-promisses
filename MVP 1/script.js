urlmessages = "https://treinamentoajax.herokuapp.com/messages"
url = "https://treinamentoajax.herokuapp.com/"

//POST
var nome = document.querySelector("#nome")
var mensagem = document.querySelector("#mensagem")
var enviar = document.querySelector("#enviar")
var lista = document.querySelector("#lista")

//GET
var obter = document.querySelector("#obter")


//POST - Enviar
enviar.addEventListener("click", e =>{
    if(nome.value == "" || mensagem == ""){
        console.log("Campos não podem estar vazio")
    }else{
        var fetchBody = {
            "message":{
                "name": nome.value,
                "message": mensagem.value
            }
        }
        var fetchConfig = {
            method: "POST",
            headers: {"Content-Type":"application/JSON"},
            body: JSON.stringify(fetchBody)
        }
        fetch(urlmessages, fetchConfig).then(console.log)
        
    }
    
})

//GET
obter.addEventListener("click", e =>{
    fetch(urlmessages).then(response => response.json())
    .then(lista => {
        for(x of lista){
            createDiv(x)
        }
    })
})

function createDiv(m){
    var divblock = document.createElement("div")
    divblock.classList.add("block")

    var nome = document.createElement("h2")
    nome.innerHTML = m.name

    var mensagem = document.createElement("p")
    mensagem.innerHTML = m.message

    divblock.appendChild(nome)
    divblock.appendChild(mensagem)
    lista.appendChild(divblock)
}

var a = {
    id: 2,
    name: "Rafael Carrilho",
    message: "Eu vou ser o hokage",
    created_at: "2020-07-19T14:41:40.852Z",
    updated_at: "2020-07-19T14:41:40.852Z"}



